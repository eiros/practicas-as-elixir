defmodule Effects do

    @moduledoc """
    This is the documentation of the `Effects` module.

    This module includes some functionalities for printing lists of `Integer` 
    numbers on the screen.
    """

    @moduledoc since: "1.0.0"

   @doc """
    Thakes a positive `Integer` `n` and prints a list ortenated from `1` to `n`.

    ## Parameters

    - n: Positive Integer that indicates the length of the list.

    ## Examples
        
    """
    @doc since: "1.0.0"
    def print(n) do
        _print(n,[])
    end

    defp _print(n,l) when (n>1) do
        _print(n-1,["\n#{to_string(n)}" | l])
    end

    defp _print(n,l) when (n==1) do
        _print(n-1,["#{to_string(n)}" | l])
    end

    defp _print(n,l) when (n<=0) do
        IO.puts(to_string(l))
    end

    @doc """
    Thakes a positive `Integer` `n` and prints a list ortenated
    of the pair values comprended between `1` to `n`.

    ## Parameters

    - n: Positive Integer that indicates the length of the list.

    ## Examples
        
    """
    @doc since: "1.0.0"
    def even_print(n) do
        _even_print(n,[])
    end

    defp _even_print(n,l) when ((n>2) and (rem(n,2)==0)) do
        _even_print(n-1,["\n#{to_string(n)}" | l])
    end

    defp _even_print(n,l) when ((n>2) and (rem(n,2)!=0)) do
        _even_print(n-1,l)
    end

    defp _even_print(n,l) when (n==2) do
        _even_print(n-1,["#{to_string(n)}" | l])
    end

    defp _even_print(n,l) when (n<=1) do
        IO.puts(to_string(l))
    end
end
