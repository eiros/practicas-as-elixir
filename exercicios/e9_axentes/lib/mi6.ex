defmodule Mi6 do
    require GenServer
    require Agent
    require Db
    require Create
    require Manipulating

    @moduledoc """
    This is the documentation of the `Mi6` module.

    This module includes functions to create an secret agency server to manage agents in order to
    operate in missions.

    This version of the `Mi6` module is implemented using `GenServer` and `Agent`s. 
    """
    @moduledoc since: "1.0.0"

    @doc """
    Creates a new `GenServer` with the `name` equals to `:mi6`.

    `Mi6:fundar() -> :ok`
    """
    @doc since: "1.0.0"
    def fundar() do
        {_,_} = GenServer.start_link(Mi6, Db.new(),name: :mi6)
        :ok
    end

    @doc false
    def init(state)  do
        {:ok,state}
    end

    
    @doc """
    Creates an `Agent` and link it with the `:mi6` Server.

    `Mi6:recrutar(axente, destino) -> :ok`

    ## Parameters:
    
    - axente: The agent name.
    - destino: The location of the agent.
    """
    @doc since: "1.0.0"
    def recrutar(axente, destino) do
        GenServer.cast(:mi6,{:recrutar, {axente,destino}})
        :ok
    end

    @doc """
    Gives a mission to an `Agent`.

    `Mi6:asignar_mision(axente, mision) -> :ok`

    ## Parameters:
    - axente: The agent who will take the mission.
    - mision: The mision given to the agent.
    """
    @doc since: "1.0.0"
    def asignar_mision(axente, mision) do
        GenServer.cast(:mi6,{:asignar, {axente, mision}})
        :ok
    end

    @doc false
    def agent_mision([head | tail], :espiar) do
        Manipulating.filter([head | tail], head)
    end

    @doc false
    def agent_mision(l, :contrainformar) do
        Manipulating.reverse(l)
    end

    @doc """
    Returns the current state of an `Agent`. 
    If the `Agent` was not found, a message will be risen.

    `Mi6:consultar_estado(axente) -> {:reply, resultado, estado} | {:reply, :you_are_here_we_are_not, state}`

    ## Parameters:
    - axente: The agent of which mission wants to be consulted.
    """
    @doc since: "1.0.0"
    def consultar_estado(axente) do
        GenServer.call(:mi6,{:consultar, axente})
    end

    @doc """
    Destroy the Agency and stops the `GenServer` 

    `Mi6:disolver() -> :ok`
    """
    @doc since: "1.0.0"
    def disolver() do
        GenServer.stop(:mi6, :normal)
        :ok
    end
    
    @doc false
    def handle_cast({:asignar, {axente, mision}}, state) do
        agent = Db.read(state, axente)
        case agent do
            {:error, :not_found} ->
                {:noreply, state}
            {:ok, pid}->
                Agent.cast(pid, Mi6, :agent_mision, [mision])
                {:noreply, state}
        end
    end

    @doc false
    def handle_cast({:recrutar, {axente, destino}}, state) do
        result = Db.read(state, axente)
        case result do
            {:ok, _} ->
                {:noreply, state}
            {:error, :not_found} ->
                length = String.length(destino)
                list = Enum.shuffle(Create.create(length))
                {:ok, pid} = Agent.start_link(fn () -> list end)
                {:noreply, Db.write(state, axente, pid)}
        end
    end

    @doc false
    def handle_call({:consultar, axente}, _from, state) do
        agent = Db.read(state, axente)
        case agent do
            {:error, :not_found} ->
                {:reply, :you_are_here_we_are_not, state}
            {:ok, pid}->
                result = Agent.get(pid, fn state -> state end)
                {:reply, result, state}
        end
    end

end
