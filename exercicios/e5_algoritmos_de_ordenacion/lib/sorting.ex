defmodule Sorting do
    @moduledoc """
    This is the documentation of the `Sorting` module.

    This module includes functions to sort lists using two different sorting algorithms: Qicksort and Mergesort.
    """
    @moduledoc since: "1.0.0"

    @doc """
    Takes a list of `numbers` `list` and sort it using the 
    quicksort algorithm: [quicksort](http://en.wikipedia.org/wiki/Quicksort).

    Uses the `head` of the given list as the pivot.

    ## Parameters

    - list: The list of `numbers` to sort.

    ## Examples
        iex>Sorting.quicksort([3, 2, 6, 5, 4, 1, 7, 9, 12])
        [1, 2, 3, 4, 5, 6, 7, 9, 12]

        iex>Sorting.quicksort([1123, 454, 3, 678, 3212, 98])
        [3, 98, 454, 678, 1123, 3212]
    """
    @doc since: "1.0.0"
    def quicksort([]), do: []
    def quicksort([head|tail]) do
        {lesser, greater} = Enum.split_with(tail, &(&1 < head))
        quicksort(lesser) ++ [head] ++ quicksort(greater)
    end

    @doc """
    Takes a list of `numbers` `list` and sort it using the 
    mergesort algorithm: [mergesort](https://en.wikipedia.org/wiki/Merge_sort).

    ## Parameters

    - list: The list of `numbers` to sort.

    ## Examples
        iex>Sorting.mergesort([3, 2, 6, 5, 4, 1, 7, 9, 12])
        [1, 2, 3, 4, 5, 6, 7, 9, 12]

        iex>Sorting.mergesort([1123, 454, 3, 678, 3212, 98])
        [3, 98, 454, 678, 1123, 3212]
    """
    @doc since: "1.0.0"
    def mergesort(list) when (length(list) <= 1), do: list
    def mergesort(list) do
        {left, right} = Enum.split(list, div(length(list), 2))
        :lists.merge( mergesort(left), mergesort(right))
    end
end
