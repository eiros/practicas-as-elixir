defmodule Measure do
    require Task
    require Manipulating
    require Sorting

    @moduledoc """
    This is the documentation of the `Measure` module.

    This module incudes the function `run` which mades a sort of benchmark for the given . 
    """
    @moduledoc since: "1.0.0"

    @doc """
    Execute the benchmark. 

    This function recives as parameters a list of functions `functions` and the number of elements to process for each function (`elements`). 
    In the first place, the function generates parallely a list of `elements` elements for every function on `functions`. Before that, it creates
    one `task` for every function on `functions` and execute the functions on `functions` simultaneouslly but in a maximun time of 10 seconds.

    The function prints a table indicating the time wasted for every function and for the data creation. If a execution excedes the 
    10 seconds it will be interrumped and indicated in the final table.

    ## Parameters:
        - functions: The list of functions to be executed.
        - elements: The number of elements to be created for the test case lists.
    """
    @doc since: "1.0.0"

    def run(functions, elements) do

        timeIni = :erlang.timestamp()
        data = createData(functions,elements)
        timeFin = :erlang.timestamp()
        timeCreation = :timer.now_diff(timeFin, timeIni) / (1000 * 1000)
        
        IO.puts "|--------------------------------------|"
        IO.puts "  Creacion de datos :  #{timeCreation} sec"
        
        tasks = executeTasks(functions, data, [])
        receiveExecutedTask(tasks, functions)
    end

    @doc false
    def createList(elements) do
        list = 1..elements |> Enum.to_list() |> Enum.map(fn _ -> (:rand.uniform(1001)-1) end) 
        list
    end

    @doc false
    def createListOfLists(elements) do
        list = 1..elements |> Enum.to_list() |> Enum.map(fn _ -> createList(elements) end)
        list
    end

    @doc false
    def createTask(:flatten, elements) do
        task = Task.async(Measure,:createListOfLists,[elements])
        task
    end

    @doc false
    def createTask(_, elements) do
        task = Task.async(Measure,:createList,[elements])
        task
    end

    @doc false
    def createTasks([{_, function} | tail], elements, taskList) do
        task = createTask(function, elements)
        createTasks(tail,elements,taskList ++ [task])
    end

    @doc false
    def createTasks([],_,taskList) do
        taskList
    end

    @doc false
    def receiveTask([head | tail], resultList) do
        receiveTask(tail,resultList ++ [Task.await(head,:infinity)]) 
    end

    @doc false
    def receiveTask([],resultList) do
        resultList
    end

    @doc false
    def createData(functions, elements) do
        taskList = createTasks(functions,elements,[])
        dataList = receiveTask(taskList,[])
        dataList
    end

    @doc false
    def executeTask({_, function}, list) do
        task = Task.async(Measure,:taskAux,[list,function])
        task
    end

    @doc false
    def executeTasks([function | tail1], [list | tail2], taskList) do
        task = executeTask(function, list)
        executeTasks(tail1,tail2,taskList ++ [task])
    end

    @doc false
    def executeTasks([],_,taskList) do
        taskList
    end

    @doc false
    def taskAux(list, :reverse) do
        timeIni = :erlang.timestamp()
        Manipulating.reverse(list)
        timeFin = :erlang.timestamp()
        timeOp = :timer.now_diff(timeFin, timeIni) / (1000 * 1000)
        timeOp
    end

    @doc false
    def taskAux(list, :flatten) do
        timeIni = :erlang.timestamp()
        Manipulating.flatten(list)
        timeFin = :erlang.timestamp()
        timeOp = :timer.now_diff(timeFin, timeIni) / (1000 * 1000)
        timeOp
    end

    @doc false
    def taskAux(list, :quicksort) do
        timeIni = :erlang.timestamp()
        Sorting.quicksort(list)
        timeFin = :erlang.timestamp()
        timeOp = :timer.now_diff(timeFin, timeIni) / (1000 * 1000)
        timeOp
    end

    @doc false
    def taskAux(list, :mergesort) do
        timeIni = :erlang.timestamp()
        Sorting.mergesort(list)
        timeFin = :erlang.timestamp()
        timeOp = :timer.now_diff(timeFin, timeIni) / (1000 * 1000)
        timeOp
    end

    @doc false
    def receiveExecutedTask(taskList, functions) do
        results = Task.yield_many(taskList, 10000)
        print_results(results, functions)
    end

    @doc false
    def print_results([{task, nil} | tail1], [{module, function} | tail2]) do
        Task.shutdown(task, :brutal_kill)
        IO.puts "  #{module}:#{function}  :  interrompida"
        print_results(tail1,tail2)
    end

    @doc false
    def print_results([{_, {:ok, value}} | tail1], [{module, function} | tail2]) do
        IO.puts "  #{module}:#{function}  :  #{value} sec"
        print_results(tail1,tail2)
    end

    @doc false
    def print_results([],[]) do
        IO.puts "|--------------------------------------|"
    end

end
