defmodule Manipulating do
    @moduledoc """
    This is the documentation of the `Manipulating` module.

    This module includes functions to manipulate lists.
    """
    @moduledoc since: "1.0.0"

    @doc """
    Given a list of `integers` `l` and an `integer` `n`,
    returns a list with the elements of `l` smaller or equals to `n`.

    ## Parameters

    - l: The list of `integers` to filter.
    - n: The `integer` to compare.

    ## Examples
        iex>Manipulating.filter([1, 2, 3, 4, 5, 6], 3)
        [1, 2, 3]

        iex>Manipulating.filter([12, 34, 5, 27, 8, 14, 11, 2], 12)
        [12, 5, 8, 11, 2]

        iex>Manipulating.filter([12, 43, 44, 32, 27], 4)
        []
    """
    @doc since: "1.0.0"
    def filter(l,n) do
        _filter(l,n,[])
    end

    defp _filter([head | tail],n,[]) do
        if (head <= n) do
            _filter(tail, n,[head])
        else
            _filter(tail,n,[])
        end
    end
    
    defp _filter([head| tail],n,l2) do
        if (head <= n) do
            _filter(tail,n,[head | l2])
        else
            _filter(tail,n,l2)
        end
    end

    defp _filter([],_,l2) do
        reverse(l2)
    end

    @doc """
    Takes a list of elements 'l' and returns a list with
    the same elementes in reverse order..

    ## Parameters

    - l: list of elemets to be reversed.

    ## Examples
        iex>Manipulating.reverse([1,2,3,4,5,6])
        [6, 5, 4, 3, 2, 1]

        iex>Manipulating.reverse([1,'b','c',[2, 3],5,6])
        [6, 5, [2, 3], 'c', 'b', 1]
    """
    @doc since: "1.0.0"
    def reverse(l) do
        _reverse(l,[])
    end

    defp _reverse([head | tail],l2) do
        _reverse(tail,[head|l2])
    end

    defp _reverse([],l2) do
        l2
    end

    @doc """
    Takes a list of lists (nested list) 'l' and returns a list result of
    concatenate all the list.

    ## Parameters

    - l: list of lists to be concatenated.

    ## Examples
        iex>Manipulating.concatenate([[1, 2,], ['a','b'],[],['c']])
        [1, 2, 'a', 'b', 'c']

        iex>Manipulating.concatenate([[1], [2,'b',3]])
        [1, 2, 'b',3]
    """
    @doc since: "1.0.0"
    def concatenate(l) do
        _concatenate(l,[])
    end

    defp _concatenate([[head | tail] | list_tail], list) do
        _concatenate(([tail | list_tail]), [head | list])
    end

    defp _concatenate([[] | tail], list) do
        _concatenate(tail, list)
    end

    defp _concatenate([head | tail], list) do
        _concatenate(tail, [head | list])
    end
    
    defp _concatenate([],l) do
        reverse(l)
    end

    @doc """
    Takes a list of nested lists 'l' and returns a list result of
    concatenate all the nested lists.

    ## Parameters

    - l: list of nested lists to be flatten.

    ## Examples
        iex>Manipulating.flatten([[1,["two",[3],[]]], [[[4]]], [:five,6]])
        [1,"two",3,4,:five,6]
    """
    
    
    def flatten([]) do
        []  
    end

    def flatten([head | tail]) do
        concatenate [flatten(head), flatten(tail)]
    end 
      
    def flatten(head) do
      [head]
    end
end
