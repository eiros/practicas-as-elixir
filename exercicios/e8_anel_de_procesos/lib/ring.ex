defmodule Ring do
    @moduledoc """
    This is the documentation of the `Ring` module.

    This module implements a `ring` of processes which creates `n`
    processes linked between them in ring form and sent a message `msg`
    `m` times starting on the first process and ending before sharing the 
    message to the next process in the ring n times.

    One time the message is sent, the proccesses stop in order.
    """
    @moduledoc since: "1.0.0"

    @doc false
    def loop(next) do
        receive do
            {m,msg} ->
                if (m > 0) do
                    send next, {m-1,msg}
                    loop(next)
                else
                    send next, {:kill}
                    loop(next)
                end
            {:kill} ->
                if ((Process.alive? next) and (self() != next)) do
                    send next, {:kill}
                    :ok
                else
                    :ok
                end
        end    
    end

    @doc false
    def _create(n,origin) do
        if (n > 0) do
           next = spawn(Ring, :_create,[n-1,origin])
           loop(next)
        else
            next = origin
            loop(next) 
        end
    end

    def _create(n,m,msg) do
        if (n > 0) do
            next = spawn(Ring, :_create,[n-1,self()])
            if (m > 0) do
                send next, {m-1,msg}
                loop(next)
             else
                 send next, {:kill}
                 loop(next)
             end
        else
            next = self()
            if (m > 0) do
                send next, {:send,m-1,msg}
                loop(next)
             else
                 send next, {:kill}
                 loop(next)
             end
        end
    end

    @doc """
    Starts the `ring` and returns `:ok`.

    ```start(n, m, msg)```
    ```:ok```
    """
    @doc since: "1.0.0"
    def start(n,m,msg) do
        spawn(Ring,:_create,[n-1,m,msg])
        :ok
    end
end
