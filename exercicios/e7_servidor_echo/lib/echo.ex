defmodule Echo do
    @moduledoc """
    This is the documentation of the `Echo` module.

    This module includes functions to manage an echo server.
    This server will forward you the messages which you send before until 
    you told the server to stop.
    """
    @moduledoc since: "1.0.0"

    @doc false
    def loop() do
        receive do
            {:start, _} ->
                loop()
            {:stop, _} ->
                Process.exit(self(),:kill)
            {:print, term, _} ->
                IO.puts term
                loop()
        end
    end

    @doc """
    Starts the `echo` server.

    `echo:start() -> :ok`
    """
    @doc since: "1.0.0"
    def start() do
        Process.register(spawn(&Echo.loop/0),:echo)
        send :echo, {:start, self()}
        :ok
   end

   @doc """
    Send to the `echo` a message to be forwarded.

    `echo:print(term) -> :ok`

    ## Parameters

    - term: the message sent to the server.
    """
    @doc since: "1.0.0"
   def print(term) do
       send :echo, {:print, term, self()}
       :ok
   end

   @doc """
    Stops the `echo` server.

    `echo:stop() -> :ok`
    """
    @doc since: "1.0.0"
   def stop() do
       send :echo, {:stop, self()}
        Process.unregister(:echo)
       :ok
   end
end
