defmodule Create do
    @moduledoc """
    This is the documentation of the `Create`module.

    This module includes functionalities to create lists.
    """

    @moduledoc since: "1.0.0"
    

    @doc """
    Thakes a positive `integer` `n` and creates a list ortenated from `1` to `n`.

    ## Parameters

    - n: Positive integer that indicates the length of the list.

    ## Examples

        iex>Create.create(7)
        [1, 2, 3, 4, 5, 6, 7]

        iex>Create.create(4)
        [1, 2, 3, 4]
    """
    @doc since: "1.0.0"
    def create(n) do
        _create(n,[])
    end

    defp _create(n,l) when (n >= 1) do 
        _create((n-1),[n | l])
    end 

    defp _create(n,l) when (n < 1) do
        l
    end

    @doc """
    Thakes a positive `integer` `n` and creates a list ortenated from `n` to `1`.

    ## Parameters

    - n: Positive integer that indicates the length of the list.

    ## Examples

        iex>Create.reverse_create(10)
        [10, 9, 8, 7, 6, 5, 4, 3, 2, 1]

        iex>Create.reverse_create(5)
        [5, 4, 3, 2, 1]
    """
    @doc since: "1.0.0"
    def reverse_create(n) do
        _reverse_create(1, n, [])
    end

    defp _reverse_create(y,n,l) when (y < n) do 
        _reverse_create((y+1), n, [y | l])
    end 

    defp _reverse_create(y,n,l) when (y == n) do
        [n | l]
    end

    defp _reverse_create(_,n,l) when (n == 0) do
        l
    end
end