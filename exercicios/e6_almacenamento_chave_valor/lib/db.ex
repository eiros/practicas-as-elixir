defmodule Db do
    @moduledoc """
    This is the documentation of the `Db` module.

    This module includes functions to manage a key-value vault.
    This version of the `Db` module is `list` based implementation.
    """
    @moduledoc since: "1.0.0"

    @doc """
    Creates a new Database and returns the reference of the created Database.

    `db:new() -> db_ref`

    ## Example:
        iex> Db.new()
        []

    """
    @doc since: "1.0.0"
    def new do
        []
    end

    @doc """
    Inserts the new `element` into the Database.

    `db:write(db_ref, key, element) -> new_db_ref`

    ## Parameters

    - db_ref: The Database reference.

    - key: The key of the new value.

    - element: The new value.

    ## Example:
        iex> Db.new() |> Db.write(5, "new") |> Db.write(7, "old") 
        [{7, "old"}, {5, "new"}]

    """
    @doc since: "1.0.0"
    def write(db_ref, key, element) do
        _write(db_ref, [{key, element}]) 
    end

    def _write([{key, element} | tail], db) do
        _write(tail, [{key, element} | db])
    end
    
    def _write([], db) do
        Manipulating.reverse(db)
    end

    @doc """
    Removes the first match of the key `key` on the vault `db_ref`

    `db:delete(db_ref, key) -> new_db_ref`

    ## Parameters

    - db_ref: The Database reference.

    - key: The key to be deleted.

    ## Example:
        iex> Db.new() |> Db.write(5, "new") |> Db.write(5, "old") |> Db.delete(5)
        [{5, "new"}]

    """
    @doc since: "1.0.0"
    def delete(db_ref, key) do
        _delete(db_ref,[],key)
    end
    
    defp _delete([],db_ref,_) do
        _delete([], db_ref)
    end

    defp _delete([{key,_}|tail],db_ref,key) do
        _delete(tail, db_ref)
    end

    defp _delete([head|tail],db_ref,key) do
        _delete(tail, [head | db_ref],key)
    end

    defp _delete([head | tail], db_ref) do
        _delete(tail, [head | db_ref])
    end

    defp _delete([], db_ref) do
        Manipulating.reverse(db_ref)
    end

    @doc """
    Retrieves the first match of the key `key` on the vault `db_ref` or an error value in case
    that not exists.

    `db:read(db_ref, key) -> {:ok, element} | {:error, :not_found}`

    ## Parameters

    - db_ref: The Database reference.

    - key: The key to found.

    ## Example:
        iex> Db.new() |> Db.write(5, "new") |> Db.write(5, "old") |> Db.read(5)
        {:ok, "old"}

    """
    @doc since: "1.0.0"
    def read(db_ref,key) do
        _read(db_ref ,key)
    end

    defp _read([{key, element}|_],key) do
        {:ok, element}
    end

    defp _read([_|tail],key) do
        _read(tail,key)
    end

    defp _read([],_) do
        {:error, :not_found}
    end

    @doc """
    Retrieves all the `keys` which contains the value `element`

    `db:match(db_ref, element) -> [key, ..., keyN]`

    ## Parameters

    - db_ref: The Database reference.

    - element: The element to match the keys.

    ## Example:
        iex> Db.new() |> Db.write(5, "new") |> Db.write(9, "old") |> Db.match("old")
        [9]

    """
    @doc since: "1.0.0"
    def match(db_ref,element) do
        _match(db_ref,element,[])
    end

    defp _match([{key, element}|tail],element,l) do
        _match(tail, element, [key | l])
    end

    defp _match([_|tail],element,l) do
        _match(tail,element,l)
    end

    defp _match([],_,l) do
        Manipulating.reverse(l)
     end

    @doc """
    Destroy the vault and returns an `:ok` value.

    `db:destroy(db_ref) -> :ok`

    ## Parameters

    - db_ref: The Database reference.

    ## Example:
        iex> Db.new() |> Db.write(5, "new") |> Db.write(5, "old") |> Db.destroy()
        :ok
    """
    @doc since: "1.0.0"
    def destroy(_db_ref) do
        :ok
    end
end
