defmodule Boolean do
    
    @moduledoc """
    This is the documentation of the `Boolean` module.

    This module includes functions to operate with boolean values.
    """
    
    @moduledoc since: "1.0.0"
    @moduledoc change: "1.0.1"

    @doc """
    Takes a Boolean (`true` or `false`) and returns its opposite.

    ## Parameters

    - bool: Boolean desired to be negated.

    ## Example

        iex> Boolean.b_not(true)
        false
    """
    @doc since: "1.0.1"
    def b_not(false) do
        true
    end

    def b_not(true) do
        false
    end

    @doc """
    Takes two Booleans (`true` or `false`) and returns its conjunction.
    ## Parameters

    - bool1: First of the Booleans of the conjunction.
    
    - bool2: Second of the Booleans of the conjunction.

    ## Examples

        iex>Boolean.b_and(false, true)
        false

        iex>Boolean.b_and(true, true)
        true
    """
    @doc since: "1.0.1"
    def b_and(true,true) do
        true
    end

    def b_and(_bool1,_bool2) do
        false
    end

    @doc """
    Takes two Booleans  (`true` or `false`) and returns its disjunction.
    ## Parameters

    - bool1: First of the Booleans of the disjunction.
    
    - bool2: Second of the Booleans of the disjunction.

    ## Examples

        iex>Boolean.b_or(false, true)
        true

        iex>Boolean.b_or(false, false)
        false
    """
    @doc since: "1.0.1"
    def b_or(false,false) do
        false
    end

    def b_or(_bool1,_bool2) do
        true
    end
end

